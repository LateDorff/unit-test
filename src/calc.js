// TODO: arithmetic operations
/**
 * Adds two numbers together
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 */
const add = (a, b) => a + b;
const subtract = (minuend, subtraend) => {
    return minuend - subtraend;
};
const multiply = (multiplier, multiplicant) => {
    return multiplier * multiplicant;
}
/**
 * Divides to two numbers
 * @param {number} dividend 
 * @param {number} divisor 
 * @returns {number}
 * @throws {Error} 0 divison
 */
const divide = (dividend, divisor) => {
    if (divisor == 0) throw new Error("0 divison is not allowed!");
    const fraction = dividend / divisor;
    return fraction;
}

export default { add, subtract, divide, multiply }